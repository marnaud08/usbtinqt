/********************************************************************************
** Form generated from reading UI file 'USBTin.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_USBTIN_H
#define UI_USBTIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableView>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_USBTin
{
public:
    QWidget *centralwidget;
    QPushButton *Ouvrir;
    QSpinBox *DLC;
    QLineEdit *Identifiant;
    QLineEdit *D0;
    QLineEdit *D1;
    QLineEdit *D2;
    QLineEdit *D3;
    QLineEdit *D4;
    QLineEdit *D5;
    QLineEdit *D6;
    QLineEdit *D7;
    QPushButton *Envoie;
    QPushButton *Effacer;
    QPushButton *Sauvegarder;
    QCheckBox *Extended;
    QCheckBox *Rtr;
    QComboBox *Vitesse;
    QComboBox *PortCom;
    QLineEdit *lineEdit;
    QPushButton *Deconnect;
    QTableView *tableView;
    QPushButton *Connecter;
    QPushButton *MCP2515;
    QLineEdit *LineAcceptance;
    QPushButton *Acceptance;
    QPushButton *AcceptanceFiltre;
    QLineEdit *lineAcceptanceFiltre;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_10;
    QPushButton *pBDeconnect;
    QMenuBar *menubar;
    QStatusBar *statusbar;
    QToolBar *toolBar;

    void setupUi(QMainWindow *USBTin)
    {
        if (USBTin->objectName().isEmpty())
            USBTin->setObjectName(QStringLiteral("USBTin"));
        USBTin->resize(800, 733);
        centralwidget = new QWidget(USBTin);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        Ouvrir = new QPushButton(centralwidget);
        Ouvrir->setObjectName(QStringLiteral("Ouvrir"));
        Ouvrir->setGeometry(QRect(380, 20, 99, 27));
        DLC = new QSpinBox(centralwidget);
        DLC->setObjectName(QStringLiteral("DLC"));
        DLC->setGeometry(QRect(160, 460, 41, 27));
        Identifiant = new QLineEdit(centralwidget);
        Identifiant->setObjectName(QStringLiteral("Identifiant"));
        Identifiant->setGeometry(QRect(30, 460, 113, 27));
        D0 = new QLineEdit(centralwidget);
        D0->setObjectName(QStringLiteral("D0"));
        D0->setGeometry(QRect(230, 460, 41, 27));
        D1 = new QLineEdit(centralwidget);
        D1->setObjectName(QStringLiteral("D1"));
        D1->setGeometry(QRect(280, 460, 41, 27));
        D2 = new QLineEdit(centralwidget);
        D2->setObjectName(QStringLiteral("D2"));
        D2->setGeometry(QRect(330, 460, 41, 27));
        D3 = new QLineEdit(centralwidget);
        D3->setObjectName(QStringLiteral("D3"));
        D3->setGeometry(QRect(380, 460, 41, 27));
        D4 = new QLineEdit(centralwidget);
        D4->setObjectName(QStringLiteral("D4"));
        D4->setGeometry(QRect(430, 460, 41, 27));
        D5 = new QLineEdit(centralwidget);
        D5->setObjectName(QStringLiteral("D5"));
        D5->setGeometry(QRect(480, 460, 41, 27));
        D6 = new QLineEdit(centralwidget);
        D6->setObjectName(QStringLiteral("D6"));
        D6->setGeometry(QRect(530, 460, 41, 27));
        D7 = new QLineEdit(centralwidget);
        D7->setObjectName(QStringLiteral("D7"));
        D7->setGeometry(QRect(580, 460, 41, 27));
        Envoie = new QPushButton(centralwidget);
        Envoie->setObjectName(QStringLiteral("Envoie"));
        Envoie->setGeometry(QRect(660, 460, 99, 27));
        Effacer = new QPushButton(centralwidget);
        Effacer->setObjectName(QStringLiteral("Effacer"));
        Effacer->setGeometry(QRect(520, 50, 99, 27));
        Sauvegarder = new QPushButton(centralwidget);
        Sauvegarder->setObjectName(QStringLiteral("Sauvegarder"));
        Sauvegarder->setGeometry(QRect(620, 50, 99, 27));
        Extended = new QCheckBox(centralwidget);
        Extended->setObjectName(QStringLiteral("Extended"));
        Extended->setGeometry(QRect(30, 490, 97, 22));
        Rtr = new QCheckBox(centralwidget);
        Rtr->setObjectName(QStringLiteral("Rtr"));
        Rtr->setGeometry(QRect(140, 490, 97, 22));
        Vitesse = new QComboBox(centralwidget);
        Vitesse->setObjectName(QStringLiteral("Vitesse"));
        Vitesse->setGeometry(QRect(270, 20, 101, 27));
        Vitesse->setAutoFillBackground(false);
        PortCom = new QComboBox(centralwidget);
        PortCom->setObjectName(QStringLiteral("PortCom"));
        PortCom->setGeometry(QRect(10, 20, 131, 27));
        lineEdit = new QLineEdit(centralwidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(40, 580, 681, 61));
        Deconnect = new QPushButton(centralwidget);
        Deconnect->setObjectName(QStringLiteral("Deconnect"));
        Deconnect->setGeometry(QRect(380, 50, 101, 23));
        tableView = new QTableView(centralwidget);
        tableView->setObjectName(QStringLiteral("tableView"));
        tableView->setGeometry(QRect(30, 90, 721, 341));
        Connecter = new QPushButton(centralwidget);
        Connecter->setObjectName(QStringLiteral("Connecter"));
        Connecter->setGeometry(QRect(150, 20, 80, 23));
        MCP2515 = new QPushButton(centralwidget);
        MCP2515->setObjectName(QStringLiteral("MCP2515"));
        MCP2515->setGeometry(QRect(700, 10, 99, 27));
        LineAcceptance = new QLineEdit(centralwidget);
        LineAcceptance->setObjectName(QStringLiteral("LineAcceptance"));
        LineAcceptance->setGeometry(QRect(200, 530, 113, 27));
        Acceptance = new QPushButton(centralwidget);
        Acceptance->setObjectName(QStringLiteral("Acceptance"));
        Acceptance->setGeometry(QRect(18, 530, 141, 27));
        AcceptanceFiltre = new QPushButton(centralwidget);
        AcceptanceFiltre->setObjectName(QStringLiteral("AcceptanceFiltre"));
        AcceptanceFiltre->setGeometry(QRect(360, 530, 141, 23));
        lineAcceptanceFiltre = new QLineEdit(centralwidget);
        lineAcceptanceFiltre->setObjectName(QStringLiteral("lineAcceptanceFiltre"));
        lineAcceptanceFiltre->setGeometry(QRect(520, 530, 113, 23));
        label = new QLabel(centralwidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(30, 440, 91, 17));
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(160, 440, 41, 20));
        label_3 = new QLabel(centralwidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(240, 440, 21, 17));
        label_4 = new QLabel(centralwidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(290, 440, 21, 17));
        label_5 = new QLabel(centralwidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(340, 440, 21, 17));
        label_6 = new QLabel(centralwidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(390, 440, 21, 17));
        label_7 = new QLabel(centralwidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(440, 440, 21, 17));
        label_8 = new QLabel(centralwidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(490, 440, 21, 17));
        label_9 = new QLabel(centralwidget);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(540, 440, 21, 17));
        label_10 = new QLabel(centralwidget);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(590, 440, 21, 17));
        pBDeconnect = new QPushButton(centralwidget);
        pBDeconnect->setObjectName(QStringLiteral("pBDeconnect"));
        pBDeconnect->setGeometry(QRect(150, 50, 89, 25));
        USBTin->setCentralWidget(centralwidget);
        menubar = new QMenuBar(USBTin);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 22));
        USBTin->setMenuBar(menubar);
        statusbar = new QStatusBar(USBTin);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        USBTin->setStatusBar(statusbar);
        toolBar = new QToolBar(USBTin);
        toolBar->setObjectName(QStringLiteral("toolBar"));
        USBTin->addToolBar(Qt::TopToolBarArea, toolBar);

        retranslateUi(USBTin);

        QMetaObject::connectSlotsByName(USBTin);
    } // setupUi

    void retranslateUi(QMainWindow *USBTin)
    {
        USBTin->setWindowTitle(QApplication::translate("USBTin", "MainWindow", Q_NULLPTR));
        Ouvrir->setText(QApplication::translate("USBTin", "Ouvrir CAN", Q_NULLPTR));
        Envoie->setText(QApplication::translate("USBTin", "Envoie", Q_NULLPTR));
        Effacer->setText(QApplication::translate("USBTin", "Effacer", Q_NULLPTR));
        Sauvegarder->setText(QApplication::translate("USBTin", "Sauvegarder", Q_NULLPTR));
        Extended->setText(QApplication::translate("USBTin", "Extended", Q_NULLPTR));
        Rtr->setText(QApplication::translate("USBTin", "Rtr", Q_NULLPTR));
        Deconnect->setText(QApplication::translate("USBTin", "Fermer", Q_NULLPTR));
        Connecter->setText(QApplication::translate("USBTin", "Connect", Q_NULLPTR));
        MCP2515->setText(QApplication::translate("USBTin", "RegMCP2515", Q_NULLPTR));
        Acceptance->setText(QApplication::translate("USBTin", "SetAcceptanceMask", Q_NULLPTR));
        AcceptanceFiltre->setText(QApplication::translate("USBTin", "SetAcceptanceFiltre", Q_NULLPTR));
        label->setText(QApplication::translate("USBTin", "Identifiant", Q_NULLPTR));
        label_2->setText(QApplication::translate("USBTin", "DLC", Q_NULLPTR));
        label_3->setText(QApplication::translate("USBTin", "D0", Q_NULLPTR));
        label_4->setText(QApplication::translate("USBTin", "D1", Q_NULLPTR));
        label_5->setText(QApplication::translate("USBTin", "D2", Q_NULLPTR));
        label_6->setText(QApplication::translate("USBTin", "D3", Q_NULLPTR));
        label_7->setText(QApplication::translate("USBTin", "D4", Q_NULLPTR));
        label_8->setText(QApplication::translate("USBTin", "D5", Q_NULLPTR));
        label_9->setText(QApplication::translate("USBTin", "D6", Q_NULLPTR));
        label_10->setText(QApplication::translate("USBTin", "D7", Q_NULLPTR));
        pBDeconnect->setText(QApplication::translate("USBTin", "Deconnect", Q_NULLPTR));
        toolBar->setWindowTitle(QApplication::translate("USBTin", "toolBar", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class USBTin: public Ui_USBTin {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_USBTIN_H
