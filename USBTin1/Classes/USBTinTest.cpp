
/*
 *  @file         USBTinTest.cpp
 *  Classe        USBTin
 *  @note         Implementation de la classe en charge des tests unitaires
 *      Author: michel arnaud  (michel.arnaud1@orange.fr)
 *
 *  Copyright (C) 2016  michel arnaud  (michel.arnaud1@orange.fr)
  *  @brief
 *  @version      0.1
 *  @date         06 septembre 2019 22:40:52
 *
 *  Description detaillee du fichier USBTinTest.cpp
 *  Fabrication   gcc (Ubuntu 5.9.5-2ubuntu1~18.04.2) QT5.9.5
 *  @todo         Liste des choses restant a faire.
 *  @bug          30 mars 2016 09:40:52 - Aucun pour l'instant
 */
// Includes system C

// Includes system C++
#include <cppunit/CompilerOutputter.h>
#include <cppunit/XmlOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>

// Includes qt

// Includes application
#include "USBTinTest.h"

// Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION(USBTinTest);

// Exemple d'assertions possibles
// # CPPUNIT_ASSERT(condition)
// Assertions that a condition is true.
// # CPPUNIT_ASSERT_MESSAGE(message, condition)
// Assertion with a user specified message.
// # CPPUNIT_FAIL(message)
// Fails with the specified message
// # CPPUNIT_ASSERT_EQUAL(expected, actual)
// Asserts that two values are equals.
// # CPPUNIT_ASSERT_EQUAL_MESSAGE(message, expected, actual)
// Asserts that two values are equals, provides additional message on failure
// # CPPUNIT_ASSERT_DOUBLES_EQUAL(expected, actual, delta)

// setUp() to initialize the variables you will use for test
void USBTinTest::setUp()
{
}

// tearDown() to release any permanent resources you allocated in setUp()
void USBTinTest::tearDown()
{
}

// Suite des tests unitaires

void USBTinTest::testConstructor()
{
    // Construction de l'instance de classe a tester
    USBTin *usbtin = new USBTin();
    CPPUNIT_ASSERT(usbtin != NULL);
    delete usbtin;
}

void USBTinTest::testUnitaire1()
{
    // Construction de l'instance de classe a tester
    USBTin *usbtin = new USBTin();
    CPPUNIT_ASSERT(usbtin != NULL);
    // Test unitaire d'une methode publique de la classe
    // Utilisation des macros CPPUNIT_ASSERT, CPPUNIT_ASSERT_EQUAL, etc.
    delete usbtin;
}

void USBTinTest::testMainView()
{
    int argc = 1;
    QApplication *app = new QApplication(argc, NULL);

    // Construction de l'instance de classe a tester
        // Construction de l'instance de la classe principale
    // déclaration de l'application
    USBTin *usbtin = new USBTin();
    CPPUNIT_ASSERT(usbtin != NULL);
 
   // Lancement de l'activité principale
   usbtin->show();

   // exécution de l'application
    app->exec();
    // Test unitaire d'une methode publique de la classe
    // Utilisation des macros CPPUNIT_ASSERT, CPPUNIT_ASSERT_EQUAL, etc.
    delete usbtin;
}


// the main method
int main(int argc, char* argv[])
{
    // informs test-listener about testresults
    CPPUNIT_NS::TestResult testresult;

    // register listener for collecting the test-results
    CPPUNIT_NS::TestResultCollector collectedresults;
    testresult.addListener(&collectedresults);

    // register listener for per-test progress output
    CPPUNIT_NS::BriefTestProgressListener progress;
    testresult.addListener(&progress);

    // insert test-suite at test-runner by registry
    CPPUNIT_NS::TestRunner testrunner;
    testrunner.addTest(CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest());
    testrunner.run(testresult);

    // output results in compiler-format
    CPPUNIT_NS::CompilerOutputter compileroutputter(&collectedresults, std::cerr);
    compileroutputter.write();

    // for hudson
    std::ofstream file( "USBTin-cppunit-report.xml" );
    CPPUNIT_NS::XmlOutputter xmloutputter(&collectedresults, file);
    xmloutputter.write();
    file.close();

    // return 0 if tests were successful
    return collectedresults.wasSuccessful() ? 0 : 1;
}

