/*
 * SerialPort.h
 *
 *  Created on: 9 févr. 2015
 *      Author: michel
 */

#ifndef SERIALPORT_H_
#define SERIALPORT_H_
#include <QtSerialPort>

#define IDLENGTHSTD 3
#define IDLENGTHEXT 8




class SerialPort:public QSerialPort {
	 Q_OBJECT
	 bool openport;
	 int nblue;
	 char *messageretour;

public:
	SerialPort(QObject *parent = 0);
	virtual ~SerialPort();
    unsigned char getNextByte();
    bool setOpenPort(QString *port);
    void setRazMessage();
    // return number of bytes in receive buffer
  //  unsigned int bytesAvailable();
    bool getOpen();
    void getMessagetoCAN(char *message);
    char getMessage0();
    // write buffer
//    int writeBuf(QByteArray *buffer);
    int writeBuf( char *buf,int l   );

 //   int OuvrirPort();

    QByteArray *dataBuffer;
    public slots:
    	void RecevoirMessage();
    signals:
    	void pourLireCAN();
    	void confirm();

};

#endif /* SERIALPORT_H_ */
