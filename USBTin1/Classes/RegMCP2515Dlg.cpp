/*
 *  Copyright (C) 2016  michel  (michel@btssn.delattre.com)
 *  @file         RegMCP2515Dlg.cpp

 *  Created on: 20 déc. 2015
 *      Author: michel arnaud  (michel.arnaud1@orange.fr)
 *
 *  Copyright (C) 2016  michel arnaud  (michel.arnaud1@orange.fr)
  *  @brief
 *  @version      0.1
 *  @date         06 septembre 2019 22:40:52
 *
 *  Description detaillee du fichier RegMCP2515Dlg.cpp
 *  Fabrication   gcc (Ubuntu 5.9.5-2ubuntu1~18.04.2) QT5.9.5
 *  @todo         Liste des choses restant a faire.
 *  @bug          30 mars 2016 09:40:52 - Aucun pour l'instant
 */


/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// Includes system C

// Includes system C++
// A décommenter si besoin cout, cin, ...
// #include <iostream>
// using namespace std;

// Includes qt

// Includes application
#include "RegMCP2515Dlg.h"
#define RXM0 0x20
#define RXM1 0x24
#define RXF0 0x00
#define RXF1 0x04
#define RXF2 0x08
#define RXF3 0x10
#define RXF4 0x14
#define RXF5 0x18

/**
 * Constructeur
 */
RegMCP2515Dlg::RegMCP2515Dlg(BusCAN *can,QObject *parent)
{
	this->setupUi(this);
	mcan=can;
	//connect(this->Ecrire,SIGNAL(currentIndexChanged(int)),this,SLOT(onVitesse(int)));
	connect(this->Ecrire,SIGNAL(clicked()),this,SLOT(onEcrire()));
	connect(this->Lire,SIGNAL(clicked()),this,SLOT(onLire()));

	this->Registre->addItem("RXM0");
	this->Registre->addItem("RXM1");
	this->Registre->addItem("RXF0");
	this->Registre->addItem("RXF1");
	this->Registre->addItem("RXF2");
	this->Registre->addItem("RXF3");
	this->Registre->addItem("RXF4");
	this->Registre->addItem("RXF5");
	this->Registreext->addItem("SIDH");
	this->Registreext->addItem("SIDL");
	this->Registreext->addItem("EID8");
	this->Registreext->addItem("EID0");
}

/**
 * Destructeur
 */
RegMCP2515Dlg::~RegMCP2515Dlg()
{
}


void RegMCP2515Dlg::onLire(){
	  QString registre=this->Registre->currentText();
	  unsigned char ext=this->Registreext->currentIndex();
	  unsigned char valreg;
	  if (registre=="RXM0")
	   valreg=0x20;
	  if (registre=="RXM1")
	   valreg=0x24;
	  if (registre=="RXF0")
	   valreg=0x0;
	  if (registre=="RXF1")
	   valreg=0x4;
	  if (registre=="RXF2")
	   valreg=0x8;
	  if (registre=="RXF3")
	   valreg=0x10;
	  if (registre=="RXF4")
	   valreg=0x14;
	  if (registre=="RXF5")
	   valreg=0x18;

	  valreg+=ext;
	  unsigned char val=mcan->getRegistre(valreg);

}

void RegMCP2515Dlg::AfficheReg(unsigned char val){
	this->lcdNumber->display(val);
}

void RegMCP2515Dlg::onEcrire(){
	  QString registre=this->Registre->currentText();
	  unsigned char ext=this->Registreext->currentIndex();
	  unsigned char valreg;
	  if (registre=="RXM0")
	   valreg=0x20;
	  if (registre=="RXM1")
	   valreg=0x24;
	  if (registre=="RXF0")
	   valreg=0x0;
	  if (registre=="RXF1")
	   valreg=0x4;
	  if (registre=="RXF2")
	   valreg=0x8;
	  if (registre=="RXF3")
	   valreg=0x10;
	  if (registre=="RXF4")
	   valreg=0x14;
	  if (registre=="RXF5")
	   valreg=0x18;

	  valreg+=ext;
	  int val=this->lineEdit->text().toInt(0,16);
	  mcan->ecrireReg(valreg,val);

}


